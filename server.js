
const express = require ("express");
var cors = require('cors');
const bodyParser = require("body-parser");
const PeopleRoutes = require("./routes/people");
const LoginRoutes = require("./routes/login");
const DesignationRoutes = require("./routes/designation");
const DepartmentRoutes = require("./routes/department");
const PerformanceRoutes = require("./routes/performance_review");
const EmployeeAssign= require("./routes/employee_assign");


const mysqlConnection = require("./connection");

var app=express();

app.use(bodyParser.json());
app.use(cors());

app.use("/people", PeopleRoutes);
app.use("/login", LoginRoutes);
app.use("/designation", DesignationRoutes);
app.use("/department", DepartmentRoutes);
app.use("/performance_review",PerformanceRoutes);
app.use("/employee_assign",EmployeeAssign);



app.listen(5000);



