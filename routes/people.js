const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const accessTokenSecret = 'shhhhh';


Router.get("/", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.user_type);
            if (user.user_type == 0) {
                mysqlConnection.query("Select id,fname,email,user_type, department, designation, last_updated_at from users", (err, rows, fields) => {
                    if (!err) {
                        res.send(rows);
                    }
                    else {
                        console.log(err);
                    }
                })
            }
            else {
                return res.sendStatus(401);
            }
        });
    } else {
        res.sendStatus(401);
    }
});

Router.get("/:id", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.user_type);
            if (user.user_type == 0) {
                mysqlConnection.query("Select id,fname,email,user_type, department, designation, last_updated_at from users where id=?", [req.params.id], (err, rows, fields) => {
                    if (!err) {
                        if(rows)
                        {
                            res.send(rows[0]);
                        }
                        else
                        {
                            return res.sendStatus(404);
                        }

                    }
                    else {
                        console.log(err);
                    }
                })

            }
            else {
                return res.sendStatus(401);
            }
        });
    } else {
        res.sendStatus(401);
    }
});


Router.post("/", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.user_type);
            if (user.user_type == 0) {
                let users = req.body;
                const salt = bcrypt.genSaltSync(saltRounds);
                const myPlaintextPassword = users.userpwd;
                let hash = bcrypt.hashSync(myPlaintextPassword, salt);
                var sql = "SET @user_id=?; SET @fname=?; SET @email=?; SET @userpwd=?; SET @user_type=?; SET @department=?; SET @designation=?; CALL Add_Update_Users(@user_id,@fname,@email, @userpwd, @user_type,@department,@designation);";
                mysqlConnection.query(sql, [0, users.fname, users.email, hash, users.user_type, users.department, users.designation], (err, rows, fields) => {
                    if (!err) {
                        if(rows.length > 0)
                        {
                            rows.forEach(element => {
                                if (element.constructor == Array) {
                                    res.header("Content-Type", 'application/json');
    
                                    res.json({ message: 'Success', user_id: element[0].id });
                                }
                            });
                            
                        }
                        else {
                            return res.sendStatus(404);
                        }
                       
                    }
                    else {
                        console.log(err);
                    }
                })
            }
            else {
                return res.sendStatus(401);
            }

        });
    } else {
        res.sendStatus(401);
    }

});



Router.put("/", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.user_type);
            if (user.user_type == 0) {
                let users = req.body;
                let pwd = '';
                var sql = "SET @user_id=?; SET @fname=?; SET @email=?; SET @userpwd=?;SET @user_type=?; SET @department=?; SET @designation=?; CALL Add_Update_Users(@user_id,@fname,@email,@userpwd,@user_type,@department,@designation);";
                mysqlConnection.query(sql, [users.user_id, users.fname, users.email, pwd, users.user_type, users.department, users.designation], (err, rows, fields) => {
                    if (!err) {
                        rows.forEach(element => {
                            if (element.constructor == Array) {
                                res.header("Content-Type", 'application/json');
                                res.json({ message: 'Success'});
                            }

                        });


                    }
                    else {
                        console.log(err);
                    }
                })
            }
            else {
                return res.sendStatus(401);
            }
        });
    } else {
        res.sendStatus(401);
    }

});


Router.delete("/:id", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.user_type);
            if (user.user_type == 0) {
                var sql = "Delete from users where id=?"
                let users = req.params.id;
                mysqlConnection.query(sql, users, (err, rows, fields) => {
                    if (!err) {

                        res.header("Content-Type", 'application/json');

                        res.json({ message: 'Delete Successful' });
                    }
                    else {
                        res.status(404).end();
                        console.log(err);
                    }
                })

            }
            else {
                return res.sendStatus(401);
            }
        });
    } else {
        res.sendStatus(401);
    }
});

module.exports = Router;