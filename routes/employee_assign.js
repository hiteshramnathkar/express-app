const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");
const jwt = require('jsonwebtoken');
const accessTokenSecret = 'shhhhh';


Router.get("/:id", (req, res) => {
    console.log(req.params);
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.email);
            var sql= "SELECT DISTINCT users.Id,users.fname,performancereview.Period FROM users Inner Join performancereview on Users.Id=performancereview.PerformanceReviewFor Where performancereview.PeerReviewdBy =? "
            mysqlConnection.query(sql, [req.params.id], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else {
                    console.log(err);
                }
            })


        });
    } else {
        res.sendStatus(401);
    }
});

Router.post("/", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, logged_user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(logged_user.user_type);
            if (logged_user.user_type == 0) {
                let users = req.body;
                users.data.forEach(function (user, index, arr) {
                    var sql = "SET @PerformanceReviewFor=?;SET @PeerReviewdBy=?;  SET @Period=?; CALL Assign_Employee(@PerformanceReviewFor, @PeerReviewdBy, @Period);";
                    mysqlConnection.query(sql, [user.PerformanceReviewFor, user.PeerReviewdBy,user.Period], (err, rows) => {
                        if (!err) {
                            console.log(rows.fieldCount);
                        }
                        else {
                            console.log(err);
                        }
                    })

                });


                return res.json({ message: 'Success' });

            }
            else {
                return res.sendStatus(401);
            }

        });
    } else {
        return res.sendStatus(401);
    }

});

module.exports = Router;