const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");
const jwt = require('jsonwebtoken');
const accessTokenSecret = 'shhhhh';

Router.get("/:user_id/:review_period", (req, res) => {
    console.log(req.params);
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.email);
            mysqlConnection.query("Select * from performancereview where PerformanceReviewFor=? AND Period=?", [req.params.user_id, req.params.review_period], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else {
                    console.log(err);
                }
            })


        });
    } else {
        res.sendStatus(401);
    }
});


Router.get("/", (req, res) => {
    console.log(req.params);
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.email);
            var sql= "SELECT DISTINCT users.Id,users.fname,performancereview.Period FROM users Inner Join performancereview on Users.Id=performancereview.PerformanceReviewFor"
            mysqlConnection.query(sql, [req.params.id], (err, rows, fields) => {
                if (!err) {
                    res.send(rows);
                }
                else {
                    console.log(err);
                }
            })


        });
    } else {
        res.sendStatus(401);
    }
});

Router.post("/", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, logged_user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(logged_user.user_type);
            if (logged_user.user_type == 0) {
                let users = req.body;
                users.data.forEach(function (user, index, arr) {
                    var Id = 0;
                    var sql = "SET @PerformanceReviewQuestion=?; SET @PerformanceReviewFor=?; SET @Rating=?; SET @Period=?;SET @Id=?; SET @QuestionId=?; CALL Add_Performance_Review(@PerformanceReviewQuestion, @PerformanceReviewFor, @Rating, @Period, @Id, @QuestionId);";
                    mysqlConnection.query(sql, [user.PerformanceReviewQuestion, user.PerformanceReviewFor, user.Rating, user.Period, Id, user.QuestionId], (err, rows) => {
                        if (!err) {
                            console.log(rows);
                        }
                        else {
                            console.log(err);
                        }
                    })

                });


                res.json({ message: 'Success'});

            }
            else {
                return res.sendStatus(401);
            }

        });
    } else {
        return res.sendStatus(401);
    }

});

Router.post("/addfeedback", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, logged_user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(logged_user.user_type);
            if (logged_user.user_type == 1) {
                let users = req.body;
                users.data.forEach(function (user, index, arr) {
                    
                    var sql = "SET @Id=?; SET @PeerFeedback=?; CALL Add_Feedback(@Id, @PeerFeedback);";
                    mysqlConnection.query(sql, [user.Id, user.PeerFeedback], (err, rows) => {
                        if (!err) {
                            console.log(rows.fieldCount);
                        }
                        else {
                            console.log(err);
                        }
                    })

                });


                return res.json({ message: 'Success' });

            }
            else {
                return res.sendStatus(401);
            }

        });
    } else {
        return res.sendStatus(401);
    }

});


Router.put("/", (req, res) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, logged_user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(logged_user.user_type);
            if (logged_user.user_type == 0) {
                let users = req.body;
                users.data.forEach(function (user, index, arr) {
                var sql = "SET @PerformanceReviewQuestion=?; SET @PerformanceReviewFor=?; SET @Rating=?;  SET @Period=?;SET @Id=?; SET @QuestionId=?; CALL Add_Performance_Review(@PerformanceReviewQuestion, @PerformanceReviewFor, @Rating, @Period, @Id, @QuestionId);";
                mysqlConnection.query(sql, [user.PerformanceReviewQuestion, user.PerformanceReviewFor, user.Rating, user.Period, user.Id, user.QuestionId], (err, rows) => {
                        if (!err) {
                            console.log(rows.fieldCount);
                        }
                        else {
                            console.log(err);
                        }
                    })
                })
                return res.json({ message: 'Success' });

            }
            else {
                return res.sendStatus(401);
            }
        });
    } else {
        res.sendStatus(401);
    }

});


module.exports = Router;