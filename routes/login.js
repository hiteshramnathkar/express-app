const express = require("express");
const Router = express.Router();
const mysqlConnection = require("../connection");
const bcrypt = require('bcrypt');
const saltRounds = 10;
var jwt = require('jsonwebtoken');
const accessTokenSecret = 'shhhhh';



Router.post("/", (req, res) => {
    let users = req.body;

    mysqlConnection.query("Select * from users where email=?", [users.email], (err, rows, fields) => {
        if (!err) {
            if (rows.length > 0) {
                if (bcrypt.compareSync(users.userpwd, rows[0].userpwd)) {
                    var token = jwt.sign({ email: users.email, user_type: rows[0].user_type, user_id: rows[0].id, name: rows[0].fname }, 'shhhhh');
                    res.header("Content-Type", 'application/json');
                    res.status(200).json({ message: 'Success', user_id: rows[0].id, name: rows[0].fname, user_type: rows[0].user_type, Auth: token });
                    // Passwords match
                } else {
                    res.header("Content-Type", 'application/json');
                    res.status(401).json({ message: 'Unauthorised' });
                    // Passwords don't match
                }

            }
            else {
                res.header("Content-Type", 'application/json');
                res.status(401).json({ message: 'Unauthorised' });
                // Passwords don't match
            }


        }
        else {
            console.log(err);
        }
    })
});


Router.get("/verify/:Auth", (req, res) => {
    const authHeader = req.params.Auth;
    if (authHeader) {
        const token = authHeader;
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            console.log(user.user_type);
            return res.json({ user_id: user.user_id, name: user.name, user_type: user.user_type, Auth: token });

        });
    } else {
        res.sendStatus(401);
    }
});

module.exports = Router;